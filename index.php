<?php

require_once 'models/Asset.php';
require_once 'models/Company.php';

$assetFurniture = new Asset('Furniture', 5000);

$company = new Company('First', 3, 500000, [$assetFurniture]);

$company->hireEmployee();
$company->hireEmployee();
$company->hireEmployee();

$company->fireEmployee();
$company->fireEmployee();

$assetOffice = new Asset('Office', 200000);
$company->buyAsset($assetOffice);

$assetCar = new Asset('Car', 10000);
$company->buyAsset($assetCar);

$company->sellAsset($assetOffice->getId());

var_dump($company);