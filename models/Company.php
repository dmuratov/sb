<?php

/**
 * Class Company
 */
class Company
{
    /**
     * Unique identifier
     *
     * @var integer
     */
    protected $id;

    /**
     * Name of asset, max length 100 characters
     *
     * @var string
     */
    protected $name;

    /**
     * Number of employees
     *
     * @var integer
     */
    protected $employees;

    /**
     * Balance
     *
     * @var float
     */
    protected $balance;

    /**
     * List of company Assets
     *
     * @var Asset[]
     */
    protected $assets;

    /**
     * Company constructor.
     * Id generates randomly
     * 
     * @param $name
     * @param int $employees
     * @param int $balance
     * @param Asset[] $assets
     */
    public function __construct($name, $employees = 1, $balance = 0, $assets = [])
    {
        $this->setId(rand(10,99).time());
        $this->setName($name);
        $this->setEmployees($employees);
        $this->setBalance($balance);
        $this->setAssets($assets);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param $balance
     * @throws Exception
     */
    public function setBalance($balance)
    {
        if ($balance < 0) {
            throw new Exception('Balance can\'t be equal to 0');
        }

        $this->balance = $balance;
    }

    /**
     * @return Asset[]
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @param Asset[] $assets
     * @throws Exception
     */
    public function setAssets($assets)
    {
        foreach ($assets as $asset) {

            if (!$asset instanceof Asset) {
                throw new Exception('Asset should be instance of Asset Class');
            }

            $this->assets[$asset->getId()] = $asset;
        }
    }

    /**
     * Increase employee count
     */
    public function hireEmployee()
    {
        $this->employees++;
    }

    /**
     * Decrease employee count
     *
     * @throws Exception
     */
    public function fireEmployee()
    {
        if ($this->employees == 1) {
            throw new Exception('Company can\'t	have 0 employees');
        }

        $this->employees--;
    }

    /**
     * @param Asset $asset
     * @throws Exception
     */
    public function buyAsset(Asset $asset)
    {
        if ($this->getBalance() - $asset->getPrice() < 0) {
            throw new Exception('Company can\'t have negative balance');
        }

        $this->setBalance($this->getBalance() - $asset->getPrice());
        $this->assets[$asset->getId()] = $asset;
    }

    /**
     * @param $assetId
     * @throws Exception
     */
    public function sellAsset($assetId)
    {
        if (!isset($this->assets[$assetId])) {
            throw new Exception('Company can\' sell assets it doesn\'t have');
        }

        $this->setBalance($this->getBalance() + $this->assets[$assetId]->getPrice());
        unlink($this->assets[$assetId]);
    }
}