<?php

/**
 * Class Asset
 */
class Asset
{
    /**
     * Unique identifier
     *
     * @var integer
     */
    protected $id;

    /**
     * Name of asset, max length 100 characters
     *
     * @var string
     */
    protected $name;

    /**
     * Price of asset
     *
     * @var float
     */
    protected $price;

    /**
     * Asset constructor.
     * Id generates randomly
     *
     * @param $name
     * @param $price
     */
    public function __construct($name, $price)
    {
        $this->setId(rand(10,99).time());
        $this->setName($name);
        $this->setPrice($price);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $price
     * @throws Exception
     */
    public function setPrice($price)
    {
        if ($price <= 0) {
            throw new Exception('Price can\'t be less or equal to 0');
        }

        $this->price = $price;
    }
}